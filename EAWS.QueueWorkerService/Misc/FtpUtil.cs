﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using EAWS.Core.Logging;

namespace EAWS.QueueWorkerService.Misc
{
    public static class FtpUtil
    {
        private static string username = "WorkerUpdater";
        private static string pass = "123456";

        private static List<string> _excludedFiles = new List<string>
        {
            "RabbitMQ",
            "Newtonsoft",
            "log4net",
            "QueueWorkerService",
            "EAWS.Core.dll",
            ".pdb",
        };

        public static DateTime GetFileCreationTime(string filePath)
        {
            WebRequest request = WebRequest.Create(filePath);
            request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
            request.Credentials = new NetworkCredential(username, pass);
            DateTime masterCreationTime;
            using (var resp = (FtpWebResponse) request.GetResponse())
            {
                masterCreationTime = resp.LastModified;
            }
            return masterCreationTime;
        }


        public static void DownloadFile(string sourceFilePath, string targetFilePath)
        {
            var request = WebRequest.Create(sourceFilePath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential(username, pass);
            using (var response = (FtpWebResponse) request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (FileStream writer = new FileStream(targetFilePath, FileMode.Create))
                    {
                        long length = response.ContentLength;
                        int bufferSize = 2048;
                        int readCount;
                        byte[] buffer = new byte[2048];

                        readCount = responseStream.Read(buffer, 0, bufferSize);
                        while (readCount > 0)
                        {
                            writer.Write(buffer, 0, readCount);
                            readCount = responseStream.Read(buffer, 0, bufferSize);
                        }

                        response.Close();
                        writer.Close();
                        responseStream.Close();
                    }
                }
            }
        }


        public static bool DownloadFtpDirectory(string url, string localPath)
        {
            var updated = false;

            NetworkCredential credentials = new NetworkCredential(username, pass);
            FtpWebRequest detailRequest = (FtpWebRequest) WebRequest.Create(url);
            detailRequest.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            detailRequest.Credentials = credentials;

            List<string> detailLines = new List<string>();

            using (FtpWebResponse listResponse = (FtpWebResponse)detailRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    detailLines.Add(listReader.ReadLine());
                }
            }


            var listRequest = (FtpWebRequest) WebRequest.Create(url);
            listRequest.Credentials = credentials;
            listRequest.Method = WebRequestMethods.Ftp.ListDirectory;
            List<string> lines = new List<string>();
            using (FtpWebResponse listResponse = (FtpWebResponse) listRequest.GetResponse())
            using (Stream listStream = listResponse.GetResponseStream())
            using (StreamReader listReader = new StreamReader(listStream))
            {
                while (!listReader.EndOfStream)
                {
                    lines.Add(listReader.ReadLine());
                }
            }

            // удаление неактуальных папок и файлов
            try
            {
                var excludeDeletePathsString = Config.ReadSetting("ExcludeToDeletePaths");
                var excludeDeletePaths = excludeDeletePathsString != null ? excludeDeletePathsString.Split(',').Select(x=>x.Trim()) : new string[0];

                var existFolders = Directory.GetDirectories(localPath);
                foreach (var existFolder in existFolders)
                {
                    var folderName = Path.GetFileName(existFolder);
                    if (!lines.Contains(folderName)
                        && !excludeDeletePaths.Contains(folderName)
                        && !_excludedFiles.Any(x => folderName.Contains(x))
                        )
                        Directory.Delete(existFolder, true);
                }

                var existFiles = Directory.GetFiles(localPath);
                foreach (var existFile in existFiles)
                {
                    var fileName = Path.GetFileName(existFile);
                    if (!lines.Contains(fileName)
                        && !excludeDeletePaths.Contains(fileName)
                        && !_excludedFiles.Any(x => fileName.Contains(x))
                        )
                        File.Delete(existFile);
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError("Ошибка при удалении папки или файла", ex);
            }


            foreach (string line in lines)
            {

                string name = line;

                if (_excludedFiles.Any(x => name.Contains(x)))
                    continue;

                string localFilePath = Path.Combine(localPath, name);
                string fileUrl = url + name;

                var index = lines.LastIndexOf(line);
                var detail = detailLines[index];
                try
                {
                    if (detail.Contains("DIR"))
                    {
                        Directory.CreateDirectory(localFilePath);
                        DownloadFtpDirectory(fileUrl + "/", localFilePath);
                    }
                    else
                    {
                        var targetFilePath = localPath + "\\" + name;
                        var masterCreationTime = GetFileCreationTime(fileUrl);
                        var currentCreationTime = File.GetLastWriteTime(targetFilePath);

                        if (currentCreationTime >= masterCreationTime)
                        {
                            continue;
                        }

                        FtpWebRequest downloadRequest = (FtpWebRequest) WebRequest.Create(fileUrl);
                        //downloadRequest.UsePassive = true;
                        //downloadRequest.UseBinary = true;
                        downloadRequest.Method = WebRequestMethods.Ftp.DownloadFile;
                        downloadRequest.Credentials = credentials;

                        using (FtpWebResponse downloadResponse = (FtpWebResponse) downloadRequest.GetResponse())
                        using (Stream responseStream = downloadResponse.GetResponseStream())
                        using (FileStream writer = new FileStream(targetFilePath, FileMode.Create))
                        {
                            int bufferSize = 2048;
                            int readCount;
                            byte[] buffer = new byte[2048];

                            if (responseStream != null)
                            {
                                readCount = responseStream.Read(buffer, 0, bufferSize);
                                while (readCount > 0)
                                {
                                    writer.Write(buffer, 0, readCount);
                                    readCount = responseStream.Read(buffer, 0, bufferSize);
                                }
                            }
                            updated = true;
                        }
                    }
                }
                catch
                {
                    var message = "Ошибка при обработке файла: <br/>"+
                        line+"<br/>"+
                        localFilePath + "<br/>"+
                        detail;
                    Logger.Instance.LogError(message);
                    throw;
                }

            }
            return updated;

        }

    }
}
