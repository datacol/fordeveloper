﻿using EAWS.QueueWorker.Workers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eaws.IntegrationTests
{
    [TestClass]
    public class ThreadTest
	{
        int jobCount = 0;
        string result = string.Empty;
        [TestMethod]
        public void ThreadTestRun()
        {
            var processCount = 9;
            for (var n = 0; n < processCount; n++)
            {
                Thread thread = new Thread(StartJob);
                thread.Start();
				Thread.Sleep(1000);
			}

			while (jobCount < processCount)
	        {
		        Thread.Sleep(1000);

		        //if (jobCount == 1)
		        //{
			       // Assert.IsTrue(result.Contains("result"));
		        //}
	        }

	        Assert.IsTrue(result.Contains("result"));

        }

		Random r = new Random(999);
		private void StartJob()
        {
	        using (var client = new WebClient())
	        {
		        // Send authorization request 
		        var authUrl = "http://ea-ws.com/token";

		        var values = new NameValueCollection();
		        values["grant_type"] = "password";
		        values["username"] = "admin@ea-ws.com";
		        values["password"] = "123456";
		        var response = client.UploadValues(authUrl, values);

		        // Get token
		        var tokenString = Encoding.Default.GetString(response);
		        var tokenObject = JObject.Parse(tokenString);
		        var authToken = tokenObject.SelectToken("token_type") + " " + tokenObject.SelectToken("access_token");

		        // Apply the token
		        client.Headers.Set("Authorization", authToken);

				// Call Api method
		        var query = "http://ea-ws.com/api/sum";

                 var apiValues = new NameValueCollection();
		        apiValues["n"] = r.Next().ToString();
		        apiValues["m"] = r.Next().ToString();
		       


		        var watch = System.Diagnostics.Stopwatch.StartNew();
		        var apiResponse = client.UploadValues(query, apiValues);
		        result = Encoding.Default.GetString(apiResponse);
				watch.Stop();
				var duration = watch.ElapsedMilliseconds;

		        Console.WriteLine("Duration: "+ duration + " Answer is: " + result);
	        }

            jobCount++;
        }
    }
}
