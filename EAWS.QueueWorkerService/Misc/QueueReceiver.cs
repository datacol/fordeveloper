using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using EAWS.Core.Logging;
using EAWS.Core.Queue;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace EAWS.QueueWorkerService.Misc
{
    public class QueueReceiver
    {
        public static void HandleQueue(object queue)
        {
            var hostName = Config.ReadSetting("queueHost");
            var login = Config.ReadSetting("queueLogin");
            var pass = Config.ReadSetting("queuePass");
            var queueName = queue.ToString();

            var factory = new ConnectionFactory() { HostName = hostName, UserName = login, Password = pass };
            string response = null;
            var apiReply = new ApiReply();
            try
            {
                var watch2 = System.Diagnostics.Stopwatch.StartNew();
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queueName,
                        durable: false,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    // ������ ������ ����������� �� ������� ��� ������� �����������
                    ushort prefetchCount;
                    if (!ushort.TryParse(Config.ReadSetting(queueName + "Prefetch"), out prefetchCount))
                        prefetchCount = ushort.Parse(Config.ReadSetting("DefaultPrefetchCount"));

                    channel.BasicQos(0, prefetchCount, false);

                    var consumer = new EventingBasicConsumer(channel); // new QueueingBasicConsumer(channel);







                    // ��������� ���������� ������ ����������� � ������
                    byte[] assemblyBytes =
                        File.ReadAllBytes(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                          "\\EAWS.QueueWorker.dll");
                    var workerDll = Assembly.Load(assemblyBytes);

                    while (true)
                    {
                        //  string response = null;
                        response = null;

                        // BasicDeliverEventArgs ea = null;

                        // ���� ������ ������� �� ��������, �� ����
                        byte[] body = null;
                        IBasicProperties props = null;
                        ulong deliveryag = 0;
                        try
                        {
                            consumer.Received += (ch, ea) =>
                            {
                                body = ea.Body;
                                props = ea.BasicProperties;
                                deliveryag = ea.DeliveryTag;
                                // ... process the message
                                channel.BasicAck(ea.DeliveryTag, false);
                            };


                            channel.BasicConsume(queue: queueName,
                                noAck: false,
                                consumer: consumer);
                        }
                        catch (Exception e)
                        {
                            Logger.Instance.LogError(e);
                            Thread.Sleep(5000);
                            continue;
                        }

                        // var body = ea.Body;

                        var replyProps = channel.CreateBasicProperties();
                        replyProps.CorrelationId = props.CorrelationId;

                        apiReply = new ApiReply();

                        try
                        {
                            var message = Encoding.UTF8.GetString(body);
                            var messageObject = JsonConvert.DeserializeObject<QueueMessage>(message);

                            // �������� ��� ������� �� ��� �������� ������
                            var workerType =
                                workerDll.GetExportedTypes().First(x => x.Name.Contains(messageObject.WorkerName));

                            // ��������� ������
                            IWorker worker = (IWorker)Activator.CreateInstance(workerType);

                            var watch = System.Diagnostics.Stopwatch.StartNew();
                            response = worker.Execute(messageObject.Parameters);
                            watch.Stop();


                            var elapsedMs = watch.ElapsedMilliseconds;
                            Logger.Instance.LogInfo("��������� ������ �������",
                                messageObject.WorkerName + " " + elapsedMs + " ��");
                            watch2.Stop();
                            apiReply.Result = string.Concat(response, "reciverwatch:'", watch2.ElapsedMilliseconds, "'");
                        }
                        catch (Exception e)
                        {
                            response = e.ToString();
                            apiReply.Error = response;
                            apiReply.ErrorCode = ErrorCode.Common;
                            Logger.Instance.LogError(e);
                        }
                        finally
                        {
                            var apiReplyString = JsonConvert.SerializeObject(apiReply);

                            var responseBytes = Encoding.UTF8.GetBytes(apiReplyString);
                            channel.BasicPublish(exchange: "",
                                routingKey: props.ReplyTo,
                                basicProperties: replyProps,
                                body: responseBytes);
                            channel.BasicAck(deliveryTag: deliveryag,
                                multiple: false);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
            }

        }

    }
}