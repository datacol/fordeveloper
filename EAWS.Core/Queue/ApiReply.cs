﻿namespace EAWS.Core.Queue
{
    /// <summary>
    /// Результат выполнения API запроса
    /// </summary>
    public class ApiReply
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public string Result { get; set; }

        /// <summary>
        /// Gets or sets the error.
        /// </summary>
        /// <value>
        /// The error.
        /// </value>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the error code.
        /// </summary>
        /// <value>
        /// The error code.
        /// </value>
        public ErrorCode ErrorCode { get; set; }
    }

    public enum ErrorCode
    {
        Common = 1,
        NeedPayment = 2,
        MethodNotFound = 3,
        Timeout
    }
}
