﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using EAWS.Core.Queue;

namespace EAWS.QueueWorker.Workers
{
    public class SumWorker : IWorker
    {
        public string Execute(Dictionary<string, object> parameters)
        {
			Thread.Sleep(5000);
            var n = Convert.ToDouble(parameters["n"], CultureInfo.InvariantCulture);
            var m = Convert.ToDouble(parameters["m"], CultureInfo.InvariantCulture);
            var result = n + m;

	        return result + "; Machine: " + Environment.MachineName;
        }
    }
}
