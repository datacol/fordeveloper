﻿using System.Collections.Generic;

namespace EAWS.Core.Queue
{
    /// <summary>
    /// Объект для взаимодействия с сервером очереди
    /// </summary>
    public class QueueMessage
    {
        
        public string WorkerName { get; set; }
        public Dictionary<string, object> Parameters { get; set; }
    }
}
