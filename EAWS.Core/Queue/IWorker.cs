﻿using System.Collections.Generic;

namespace EAWS.Core.Queue
{
    public interface IWorker
    {
        string Execute(Dictionary<string, object> parameters);
    }
}
