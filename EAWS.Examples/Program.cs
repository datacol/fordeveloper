﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using EAWS.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EAWS.Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var client = new WebClient())
            {
                // Отправляем запрос на аутентификацию с последующим получением токена
                var authUrl = "http://ea-ws.com/token";

                var values = new NameValueCollection();
                values["grant_type"] = "password";
                values["username"] = "t@t.com";
                values["password"] = "1";
                var response = client.UploadValues(authUrl, values);
                
                // Вычисляем токен
                var tokenString = Encoding.Default.GetString(response);
                var tokenObject = JObject.Parse(tokenString);
                var authToken = tokenObject.SelectToken("token_type") + " " + tokenObject.SelectToken("access_token");

                // Применяем токен
                client.Headers.Set("Authorization", authToken);

                // Вызываем Api метод
                var query = "http://ea-ws.com/api/sum";
                var apiValues = new NameValueCollection();
                apiValues["n"] = "1.3";
                apiValues["m"] = "2.4";

                var apiResponse = client.UploadValues(query, apiValues);
                var apiResponseString = Encoding.Default.GetString(apiResponse);
                
                Console.WriteLine("Answer is:" + apiResponseString);
                Console.WriteLine("Well done!");
            }
        }
    }
}
