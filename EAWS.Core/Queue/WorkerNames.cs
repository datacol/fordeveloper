﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAWS.Core.Queue
{
    /// <summary>
    /// Имя константы должно совпадать с именем класса обработчика, который находится в папке Workers проекта EAWS.QueueWorker
    /// </summary>
    public class WorkerNames
    {
        public const string Sum = "Sum";
    }
}
