using System;

namespace EAWS.Core.Logging
{
    public static class LoggerConfig
    {
        [ThreadStatic]
        private static bool sendEmail = true;

        public static bool SendEmail
        {
            get { return sendEmail; }
            set { sendEmail = value; }
        }
    }
}

