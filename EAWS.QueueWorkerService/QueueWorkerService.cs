﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using EAWS.Core.Logging;
using EAWS.QueueWorkerService.Misc;

namespace EAWS.QueueWorkerService
{
    public partial class QueueWorkerService : ServiceBase
    {
        private CancellationTokenSource tokenSource;
        private readonly List<Thread> threads = new List<Thread>();

        public QueueWorkerService()
        {
            InitializeComponent();
        }

        public void Run()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                Logger.Instance.LogInfo("Старт сервиса");

                // обновим все необходимые конфиги и dll
                string queues = null;
                while (queues == null)
                {
                    Updater.Run();
                    queues = Config.ReadSetting("queues");
                    if(queues == null)
                        Thread.Sleep(1000);
                }
                
                var queuesArray = queues.Split(',');

                foreach (var queue in queuesArray)
                {
                    var thread = new Thread(QueueReceiver.HandleQueue);
                    threads.Add(thread);
                    thread.Start(queue);
                }

                var updateThread = new Thread(Updater.ContinueUpdater);
                threads.Add(updateThread);
                updateThread.Start();
            }
            catch(Exception ex) 
            {
                Logger.Instance.LogError(ex);
                throw;
            }
        }

        protected override void OnStop()
        {
            foreach (var thread in threads)
            {
                thread.Abort();
            }
        }
    }
}
