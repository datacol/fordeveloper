using System;
using log4net;

namespace EAWS.Core.Logging
{
    static class LoggerExtension
    {
        public static Exception Exception(this ILog logger, Exception exception, string fmt, params object[] args)
        {
            logger.Error(string.Format(fmt, args), exception);
            return exception;
        }
    }



}