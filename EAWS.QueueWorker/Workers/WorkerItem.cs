﻿using System;
using System.Text;

namespace EAWS.QueueWorker.Workers
{
    public class WorkerItem
    {
        public static int Sum(string message)
        {
            var items = message.Split('|');
            var n = Convert.ToInt32(items[0]);
            var m = Convert.ToInt32(items[1]);
            return n + m;
        }
    }
}
