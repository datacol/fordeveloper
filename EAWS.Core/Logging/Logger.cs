using System;
using System.IO;
using System.Threading;
using log4net;

namespace EAWS.Core.Logging
{
    public class Logger
    {
        private static readonly Lazy<Logger> Lazy = new Lazy<Logger>(() => new Logger(), LazyThreadSafetyMode.PublicationOnly);
        private Logger()
        {
            log = LogManager.GetLogger("EAWS");
            log4net.Config.XmlConfigurator.Configure();
        }

        public static Logger Instance { get { return Lazy.Value; } }

        private static ILog log;

        public void LogError(string message)
        {
            log.Error(message);
        }

        public void LogError(string message, Exception exception)
        {
            log.Error(message, exception);
        }

        public void LogError(Exception exception)
        {
            log.Error(exception);
        }

        public void LogError(string subject, string data, Exception exception)
        {
            var message = GetMessage(subject, data);
            log.Error(message, exception);
        }

        public void LogInfo(string info, bool sendEmail = true)
        {
            //var originalVal = LoggerConfig.SendEmail;
            //LoggerConfig.SendEmail = sendEmail;
            log.Info(info);
            //LoggerConfig.SendEmail = originalVal;
        }

        public void LogInfo(string message, Exception exception)
        {
            log.Info(message, exception);
        }

        public void LogInfo(string subject, string data)
        {
            //var originalVal = LoggerConfig.SendEmail;
            //LoggerConfig.SendEmail = true;
            var message = GetMessage(subject, data);
            log.Info(message);
            //LoggerConfig.SendEmail = originalVal;
        }


        private static string GetMessage(string subject, string data)
        {
            var message = string.Format("{0} {1}{1}{2}", subject, Environment.NewLine, data);
            return message;
        }


    }
}