using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;

namespace EAWS.Core.Logging
{
    public class HtmlSmtpAppender : SmtpAppender
    {
        public ILayout SubjectLayout
        {
            get;
            set;
        }

        override protected void Append(LoggingEvent loggingEvent)
        {
            //if (!LoggerConfig.SendEmail) return;

            try
            {
                var writer = new StringWriter(System.Globalization.CultureInfo.InvariantCulture);

                string t = Layout.Header;
                if (t != null)
                {
                    writer.Write(t);
                }

                // Render the event and append the text to the buffer
                RenderLoggingEvent(writer, loggingEvent);


                t = Layout.Footer;
                if (t != null)
                {
                    writer.Write(t);
                }

                var mailMessage = new MailMessage(From, To)
                {
                    IsBodyHtml = true,
                    From = new MailAddress(From)
                };

                var body = writer.ToString();
                body = body.Replace(Environment.NewLine, "<br />");

                mailMessage.Body = body;
                

                if (SubjectLayout == null)
                {
                    mailMessage.Subject = "Missing Subject Layout";
                }
                else
                {
                    var subjectWriter = new StringWriter(System.Globalization.CultureInfo.InvariantCulture);
                    SubjectLayout.Format(subjectWriter, loggingEvent);

                    var subject = subjectWriter.ToString();
                    
                    mailMessage.Subject = subject;

                }

                if (SmtpHost != null && SmtpHost.Length > 0)
                {
                    var client = new SmtpClient(SmtpHost);
                    client.EnableSsl = EnableSsl;
                    client.Credentials = new NetworkCredential(Username, Password);
                    Task.Factory.StartNew(() => client.Send(mailMessage));
                }
            }
            catch (Exception e)
            {
                ErrorHandler.Error("Error occurred while sending e-mail notification.", e);
            }
        }
    }
}