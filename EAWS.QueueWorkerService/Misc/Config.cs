﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EAWS.Core.Logging;

namespace EAWS.QueueWorkerService.Misc
{
    public class Config
    {

        private static Configuration configuration;

        public static void ApplyConfig()
        {
            var configFullPath = Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile)+"\\Queue.config";

            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap {ExeConfigFilename = configFullPath};
            configuration = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
        }

        public static string ReadSetting(string key)
        {
            try
            {
                var appSettings = configuration.AppSettings;
                return appSettings.Settings[key] != null ? appSettings.Settings[key].Value : string.Empty;
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
                return null;

            }
        }



    }



}
