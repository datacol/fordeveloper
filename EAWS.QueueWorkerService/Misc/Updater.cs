﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using EAWS.Core.Logging;

namespace EAWS.QueueWorkerService.Misc
{
    public class Updater
    {
        public static void Run()
        {
            try
            {
                var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                Config.ApplyConfig();
                var updated = FtpUtil.DownloadFtpDirectory("", path);
                Config.ApplyConfig();

                if (updated)
                {
                    // при обновлении конфига рестартуем себя.
                    Logger.Instance.LogInfo("Перезагрузка при обновлении конфига");

                    Restart();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.LogError(ex);
                Logger.Instance.LogInfo("Перезагрузка при ошибке обновления");
                Restart();

            }
        }

        private static void Restart()
        {
            //const string strCmdText = "/C net stop \"EAWS.QueueWorkerService\"&net start \"EAWS.QueueWorkerService\"";
            //Process.Start("CMD.exe", strCmdText);
            Environment.Exit(1);
        }


        // Метод для фонового обновления dll
        public static void ContinueUpdater()
        {
            while (true)
            {
                Run();
                Thread.Sleep(5000);
            }
        }
    }
}
